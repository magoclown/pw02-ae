class Shape {
    public width: number;
    public height: number;

    public area(): number {
        return null;
    }

    public perimeter(): number {
        return null;
    }
}

class Circle extends Shape {
    public radio: number;

    public area(): number {
        return Math.PI * this.radio * this.radio;
    }
}