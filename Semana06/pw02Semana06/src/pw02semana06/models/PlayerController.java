/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pw02semana06.models;

/**
 * Clase para control de jugador
 *
 * @author magoc
 */
public class PlayerController extends CharacterController {

    public PlayerController() {
    }

    /**
     * Contructor que ingresa el nombre y edad del jugador
     * @param name Nombre del jugador
     * @param age Edad del jugador
     */
    public PlayerController(String name, int age) {
        super(name, age);
    }

    public PlayerController(String name, long size, int age) {
        super(name, size, age);
    }

    /**
     * Metodo para mover al jugador
     */
    @Override
    public void move() {
        super.move(); //To change body of generated methods, choose Tools | Templates.
        System.out.println("Move PlayerController");
    }
}
