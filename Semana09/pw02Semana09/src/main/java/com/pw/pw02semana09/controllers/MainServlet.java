/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pw.pw02semana09.controllers;

import com.pw.pw02semana09.models.Card;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author magoc
 */
@WebServlet(name = "MainServlet", urlPatterns = {"/MainServlet"})
public class MainServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
//        response.setContentType("text/html;charset=UTF-8");
//        try (PrintWriter out = response.getWriter()) {
//            /* TODO output your page here. You may use following sample code. */
//            out.println("<!DOCTYPE html>");
//            out.println("<html>");
//            out.println("<head>");
//            out.println("<title>Servlet MainServlet</title>");            
//            out.println("</head>");
//            out.println("<body>");
//            out.println("<h1>Servlet MainServlet at " + request.getContextPath() + "</h1>");
//            out.println("</body>");
//            out.println("</html>");
//        }
        List<Card> cards = new ArrayList();
        // Informacion Mock
        // Informacion de Prueba
        cards.add(new Card("https://cdnb.artstation.com/p/assets/images/images/030/835/481/4k/brice-laville-saint-martin-electro-giant-002.jpg?1601815509",
                "Electro Giant",
                "I had the pleasure to create the Electro Giant in 3D and being the Co-Director and Creative Director of this Cinematic.",
                "https://www.artstation.com/artwork/nY2EQ6"));
        cards.add(new Card("https://cdnb.artstation.com/p/assets/images/images/030/495/339/large/kati-starsoulart-starsoulart-skatercolor3-800px3.jpg?1600784827",
                "Prince of the Ice Rink ",
                "My owl prince made for CDChallenge Animal Skatepark He's an owl on ice!",
                "https://www.artstation.com/artwork/mDWzod"));
        cards.add(new Card("https://cdnb.artstation.com/p/assets/images/images/030/528/353/4k/brahim-bensehoul-1.jpg?1600874748",
                "Batman fashion",
                "Follow me on Instagram for sketches and more art:",
                "https://www.artstation.com/artwork/D5m9bO"));
        request.setAttribute("title", "ForEach");
        request.setAttribute("cards", cards);
        request.getRequestDispatcher("index.jsp").forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
