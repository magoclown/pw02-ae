<%-- 
    Document   : index
    Created on : 30/10/2020, 08:03:14 PM
    Author     : magoc
--%>

<%@page import="com.pw.pw02semana09.models.Card"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    // Conversion implicita
    // Explicita
    // Aqui nosotros requerimos una casting explicito
    String title = (String) request.getAttribute("title");
    List<Card> cards = (List<Card>) request.getAttribute("cards");
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="assets/css/main.css"/>
    </head>
    <body>
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="#">Navbar</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="MainServlet">Link</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Dropdown
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="#">Action</a>
                            <a class="dropdown-item" href="#">Another action</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="#">Something else here</a>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Disabled</a>
                    </li>
                </ul>
                <%
                    if (1 == 0) {
                %>
                <form class="form-inline my-2 my-lg-0">
                    <input class="form-control mr-sm-2" type="text" placeholder="User">
                    <input class="form-control mr-sm-2" type="password" placeholder="Password">
                    <button class="btn btn-outline-success my-2 my-sm-0" type="submit">LogIn</button>
                </form>
                <%
                } else {
                %>
                <img style="width: 30px" src="https://cdnb.artstation.com/p/assets/images/images/030/779/423/4k/luigi-lucarelli-night-sky-s.jpg?1601624829">Jose
                <%
                    }
                %>
            </div>
        </nav>
        <div class="container">
            <h1>Mi Primer JSP</h1>
            <h2><%= title%></h2>
            <div class="row">
                <%
                    if (cards != null) {
                        // (TipoDeDato NombreDeIteracion : Lista)
                        for (Card card : cards) {
                %>
                <div class="card col-4" >
                    <img src="<%= card.getUrlImage()%>" style="max-height: 200px" alt="...">
                    <div class="card-body">
                        <h5 class="card-title"><%= card.getTitle()%></h5>
                        <p class="card-text"><%= card.getDescription()%></p>
                        <a href="<%= card.getLinkRef()%>" taget="_blank" class="btn btn-primary">Go somewhere</a>
                    </div>
                </div>
                <%
                        }
                    }
                %>
            </div>
            <h2>Cards</h2>
            <div class="row">
                <%
                    for (int i = 0; i < 6; i++) {
                %>
                <div class="card col-4" >
                    <img src="https://cdnb.artstation.com/p/assets/images/images/030/288/391/large/parth-shah-5.jpg?1600166561" class="card-img-top" alt="...">
                    <div class="card-body">
                        <h5 class="card-title">Jack Black</h5>
                        <p class="card-text">Hello Everyone, I would like to share this my new Character model of Jack Black. He is one of the best actor.</p>
                        <a href="https://www.artstation.com/artwork/xJBxaY" taget="_blank" class="btn btn-primary">Go somewhere</a>
                    </div>
                </div>
                <%
                    }
                %>
            </div>
            <h2>For</h2>
            <ul>
                <%
                    for (int i = 0; i < 10; i++) {
                %>
                <li>Categoria <%= i%></li>
                    <%
                        }
                    %>
                <!--                <li>Categoria 2</li>
                                <li>Categoria 3</li>-->
            </ul>
        </div>
    </body>
</html>
