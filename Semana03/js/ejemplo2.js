function hola(name = "Jose") {
  console.log("Helo " + name);
  // alert(`Hello ${name}`);
}

hola("Manuel");

function hello(callback) {
  console.log("Hello");
  callback();
}

function world() {
  console.log("World");
}

hello(world);

let suma = (a, b) => a + b;
console.log(suma(5,3));

// = Asignacion 
// == Comparacion de Valor
// === Comparacion de Valor y Tipo

var req = new XMLHttpRequest();
req.onreadystatechange = function() {
    if(this.readyState === 4 && this.status === 200) {
        console.log(this.responseText);
    }
}
req.open('GET','https://api.openbrewerydb.org/breweries');
req.send();

// try {

// }
// catch {

// }
// finally { }