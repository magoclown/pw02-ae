// SCOPE Global
// SCOPE Bloque


function hola() {
  if (true) {
    //Global
    var hello = "Hola";
    console.log(hello);
    //Bloque
    let adios = "adios";
    console.log(adios);
  }
  console.log(hello);
  adios();
  console.log(adios);
}

function adios() {
    const hello = "test";
    console.log(hello);
}

function dataType() {
    let number = 5;
    let string = "5";
    // number = string;
    console.log(number + string);
    console.log(number * "2");
    console.log(number + +string);
    console.log(number + -"2");
}

function ciclos() {
    let numbers = [5,1,7,9,5,4,3,8,7,6,8,4];
    console.log("FOR");
    for(let i = 0; i < numbers.length; i++) {
        console.log(numbers[i]);
    }
    console.log("FOR IN");
    for(let number in numbers) {
        console.log(number);
    }
    console.log("FOR OF");
    for(let n of numbers) {
        console.log(n);
    }
}

// hola();
// adios();
// dataType();
ciclos();