/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pw02semana05;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author magoc
 */
public class Pw02Semana05 {

    // Private: Unicamente la clase donde fue declarada
    private int size;
    // Default: Sobre la misma clase, y heredacion sobre el mismo package
    Double height;
    // Protected: SObre misma clase, y heredeacion sobre mismo y otros package
    protected String name;
    // Public: Sobre misma clase y otras clases de otros package
    public String test;
            
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Pw02Semana05 test = new Pw02Semana05();
        test.name = "Jose";
        System.out.println("Name: " + test.name);
        // sout ctrl + barra espaciadora despues enter o tabulador
        
        List<Integer> ids = new ArrayList();
        ids.add(5);
        ids.add(3);
        ids.add(8);
        ids.add(5);
        ids.add(13);
//        ids.remove(2);
        // for each
        for (Integer id : ids) {
            System.out.println(id);
        }
        Map<Integer, String> dictionary = new HashMap<>();
        dictionary.put(24, "Jose");
        dictionary.put(12, "Aimee");
        dictionary.put(16, "Bryan");
        System.out.println("D12: " + dictionary.get(12)); 
        for (Map.Entry<Integer, String> entry : dictionary.entrySet()) {
            Object key = entry.getKey();
            Object val = entry.getValue();
            System.out.println("K: " + key + ",V:" + val);            
        }
        if(dictionary.containsValue("Jose")) {
            //Si existe Valor
        }
        if(dictionary.containsKey(16)) {
            //Si existe Llave
        }
        System.out.println("2 es Impar? " + OddCalculator.isOdd(2));
        System.out.println("3 es Impar? " + OddCalculator.isOdd(3));
        FibonacciCalculator.calculateFibonacci(20);
    }
    
}
