/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pw02semana07.Managers;

import pw02semana07.Controllers.PlayerController;
import pw02semana07.Enums.Levels;

/**
 *
 * @author magoc
 */
public final class GameManager {

    private static final GameManager instance;

    static {
        instance = new GameManager();
    }

    public GameManager() {
    }

    public static GameManager getInstance() {
        return instance;
    }

    private Levels level;
    private int score;
    private PlayerController player;

    public Levels getLevel() {
        return level;
    }

    public void setLevel(Levels level) {
        this.level = level;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public PlayerController getPlayer() {
        return player;
    }

    public void setPlayer(PlayerController player) {
        this.player = player;
    }

    public void LoadGame() {
        this.level = Levels.Main_Scene;
    }

    public void LoadTutorial() {
        this.level = Levels.Tutorial_Scene;
    }

    public void initGame() {
        this.level = Levels.Water_Level;
        this.player = new PlayerController(100, false);
        this.score = 0;
    }
}
