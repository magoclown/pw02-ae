/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pw02semana07.Enums;

/**
 * Enumeracion para los niveles
 *
 * @author magoc
 */
public enum Levels {
    Main_Scene,
    Tutorial_Scene,
    Water_Level,
    Water_Boss_Level,
    Credits,
    Thanks_For_Playing
}
