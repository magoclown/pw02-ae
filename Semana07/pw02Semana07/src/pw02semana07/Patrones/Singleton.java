/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pw02semana07.Patrones;

/**
 * Patron de diseño que nos permite generar una sola instancia
 *
 * @author magoc
 */
public class Singleton {
    // Instancia que sera mandada a llamar
    private static final Singleton instance;

    // Permite inicializar las propiedades estaticas
    static {
        instance = new Singleton();
    }

    public Singleton() {
    }

    public static Singleton getInstance() {
        return instance;
    }

}
