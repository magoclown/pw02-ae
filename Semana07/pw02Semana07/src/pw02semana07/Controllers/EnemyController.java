/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pw02semana07.Controllers;

import pw02semana07.Managers.GameManager;

/**
 *
 * @author magoc
 */
public class EnemyController extends AIController {

    private int score;

    public EnemyController() {
    }

    public EnemyController(int score) {
        this.score = score;
    }

    public EnemyController(int score, double health, boolean isDeath) {
        super(health, isDeath);
        this.score = score;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    @Override
    public void takeDamage(double amount) {
        super.takeDamage(amount); //To change body of generated methods, choose Tools | Templates.
        System.out.println("Enemy Health:" + getHealth());
    }

    @Override
    public void attack() {
        super.attack(); //To change body of generated methods, choose Tools | Templates.
        System.out.println("Enemy Attack");
        GameManager.getInstance().getPlayer().takeDamage(5);
    }

    @Override
    public void death() {
        super.death(); //To change body of generated methods, choose Tools | Templates.
        System.out.println("Enemy Death");
        GameManager.getInstance().setScore(score);
    }

}
