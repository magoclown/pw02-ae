/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pw02semana07.Controllers;

/**
 *
 * @author magoc
 */
public class PlayerController extends CharacterController {

    public PlayerController() {
    }

    public PlayerController(double health, boolean isDeath) {
        super(health, isDeath);
    }

    public PlayerController(PlayerController player) {
        this.setHealth(player.getHealth());
        this.setIsDeath(player.isIsDeath());
    }

    @Override
    public void takeDamage(double amount) {
        super.takeDamage(amount); //To change body of generated methods, choose Tools | Templates.
        System.out.println("Player Health:" + getHealth());
    }
    
    

    @Override
    public void move() {
        System.out.println("Player Move");
    }

    @Override
    public void attack() {
        System.out.println("Player Attack");
    }

    @Override
    public void death() {
        super.death(); //To change body of generated methods, choose Tools | Templates.
        System.out.println("Player Death");
    }

}
